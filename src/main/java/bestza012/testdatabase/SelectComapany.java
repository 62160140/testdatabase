/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bestza012.testdatabase;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.SQLOutput;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author WIN10
 */
public class SelectComapany {

    public static void main(String[] args) {

        Connection c = null;
        String dbname = "filedb.db";
        Statement stmt = null;

        try {
            Class.forName("org.sqlite.JDBC");
            c = DriverManager.getConnection("jdbc:sqlite:" + dbname);
            c.setAutoCommit(false);
            stmt = c.createStatement();
            String sql = "SELECT * FROM COMPANY;";
            ResultSet result = stmt.executeQuery(sql);
            while(result.next()){
                System.out.println(result.getString("ID"));
                System.out.println(result.getString("NAME"));
                System.out.println(result.getString("AGE"));
                System.out.println(result.getString("ADDRESS"));
                System.out.println(result.getString("SARALY"));
            }
            c.commit();
            stmt.close();
            c.close();
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(InsertUser.class.getName()).log(Level.SEVERE, null, ex);
        } catch (SQLException ex) {
            Logger.getLogger(InsertUser.class.getName()).log(Level.SEVERE, null, ex);
        }

    }
}
